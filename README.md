## IEnumerable
### Description (RU)
Для интерфейса IEnumerable<T> реализовать расширающие функции 
* Cast,
* Where,
* Select,
* SelectMany,
* All,
* Any,
* First,
* FirstOrDefault,
* Intersect,
* Skip,
* Take,  
функционал которых повторяет функционал функций в LINQ (без перегрузок,
достаточно одного варианта), используя стандартные делегаты.  
Продемонстировать работу со всеми функциями.