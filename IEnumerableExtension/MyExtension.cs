﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IEnumerableExtension
{
    public static class MyExtension
    {

        /// <summary>
        /// Selects values that are based on a predicate function.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="collection"></param>
        /// <param name="predicate"></param>
        /// <returns>new IEnumerable<typeparamref name="T"/></returns>
        /// <remarks>https://msdn.microsoft.com/ru-ru/library/mt693055.aspx</remarks>  
        public static IEnumerable<T> MyWhere<T>(this IEnumerable<T> collection, Predicate<T> predicate)
        {
            foreach (var item in collection)
                if (predicate(item))
                    yield return item;
        }

        /// <summary>
        /// Returns the set intersection, which means elements that appear in each of two collections.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="firstCollection">First collection</param>
        /// <param name="secondCollection">Second collection</param>
        /// <returns>new IEnumerable<typeparamref name="T"/></returns>
        /// <remarks>https://msdn.microsoft.com/ru-ru/library/mt693100.aspx</remarks>
        public static IEnumerable<T> MyIntersect<T>(this IEnumerable<T> firstCollection, IEnumerable<T> secondCollection)
        {
            foreach (var firstItem in firstCollection)
                foreach (var secondItem in secondCollection)
                    if (firstItem.Equals(secondItem))
                        yield return firstItem;
        }

        /// <summary>
        /// Determines whether all the elements in a sequence satisfy a condition.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="collection"></param>
        /// <param name="predicate"></param>
        /// <returns>
        ///   <c>true</c> if all the elements satisfy a condition; otherwise, <c>false</c>.
        /// </returns>
        /// <remarks>https://msdn.microsoft.com/ru-ru/library/mt693039.aspx</remarks>
        public static bool MyAll<T>(this IEnumerable<T> collection, Predicate<T> predicate)
        {
            foreach (var item in collection)
                if (!predicate(item))
                    return false;
            return true;
        }

        /// <summary>
        /// Determines whether any elements in a sequence satisfy a condition.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="collection"></param>
        /// <param name="predicate"></param>
        /// <returns>
        ///   <c>true</c> if any elements satisfy a condition; otherwise, <c>false</c>.
        /// </returns>
        /// <remarks>https://msdn.microsoft.com/ru-ru/library/mt693039.aspx</remarks>
        public static bool MyAny<T>(this IEnumerable<T> collection, Predicate<T> predicate)
        {
            foreach (var item in collection)
                if (!predicate(item))
                    return true;
            return false;
        }

        /// <summary>
        /// Projects values that are based on a transform function.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <typeparam name="G"></typeparam>
        /// <param name="collection"></param>
        /// <param name="func"></param>
        /// <returns>new IEnumerable<typeparamref name="G"/></returns>
        /// <remarks>https://msdn.microsoft.com/ru-ru/library/mt693038.aspx</remarks>
        public static IEnumerable<G> MySelect<T, G>(this IEnumerable<T> collection, Func<T, G> func)
        {
            foreach (var item in collection)
                yield return func(item);
        }

        /// <summary>
        /// Projects sequences of values that are based on a transform function and then flattens them into one sequence.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <typeparam name="G"></typeparam>
        /// <param name="collection"></param>
        /// <param name="func"></param>
        /// <returns>new IEnumerable<typeparamref name="G"/></returns>
        /// <remarks>https://msdn.microsoft.com/ru-ru/library/mt693038.aspx</remarks>
        public static IEnumerable<G> MySelectMany<T, G>(this IEnumerable<T> collection, Func<T, IEnumerable<G>> func)
        {
            foreach (var collitem in collection)
                foreach (var item in func(collitem))
                    yield return item;
        }

        /// <summary>
        /// Skips elements up to a specified position in a sequence.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="collection"></param>
        /// <param name="count"></param>
        /// <returns>new IEnumerable<typeparamref name="T"/></returns>
        /// <remarks>https://msdn.microsoft.com/ru-ru/library/mt693063.aspx</remarks>
        public static IEnumerable<T> MySkip<T>(this IEnumerable<T> collection, int count)
        {
            int i = 0;
            foreach (var item in collection)
            {
                if (i >= count)
                    yield return item;
                i++;
            }
        }


        /// <summary>
        /// Takes elements up to a specified position in a sequence.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="collection"></param>
        /// <param name="count"></param>
        /// <returns>new IEnumerable<typeparamref name="T"/></returns>
        /// <remarks>https://msdn.microsoft.com/ru-ru/library/mt693063.aspx</remarks>
        public static IEnumerable<T> MyTake<T>(this IEnumerable<T> collection, int count)
        {
            int i = 0;
            foreach (var item in collection)
            {
                if (i < count)
                    yield return item;
                i++;
            }
        }

        /// <summary>
        /// Returns the first element of a collection
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="collection"></param>
        /// <returns>First <typeparamref name="T"/> element</returns>
        /// <remarks>https://msdn.microsoft.com/ru-ru/library/mt693060.aspx</remarks>
        public static T MyFirst<T>(this IEnumerable<T> collection)
        {
            int i = 0;
            foreach (var item in collection)
            {
                if (i == 0)
                    return item;
            }
            throw new Exception("Empty collection");

            /*
            IEnumerator<T> enumerator = collection.GetEnumerator();
            if (!enumerator.MoveNext()) throw new Exception("Empty collection");
            else return enumerator.Current;
            */
        }

        /// <summary>
        /// Returns the first element of a collection, or a default value if no such element exists.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="collection"></param>
        /// <returns>First <typeparamref name="T"/> element or default of <typeparamref name="T"/></returns>
        /// <remarks>https://msdn.microsoft.com/ru-ru/library/mt693060.aspx</remarks>
        public static T MyFirstOrDefault<T>(this IEnumerable<T> collection)
        {
            int i = 0;
            foreach (var item in collection)
            {
                if (i == 0)
                    return item;
            }
            return default(T);

            /*
                IEnumerator<T> enumerator = collection.GetEnumerator();
                if (!enumerator.MoveNext()) return default(T);
                else return enumerator.Current;
            */
        }

        /// <summary>
        /// Casts the elements of a collection to a specified type.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="collection"></param>
        /// <returns>new IEnumerable<typeparamref name="T"/></returns>
        /// <remarks>https://msdn.microsoft.com/ru-ru/library/mt693053.aspx</remarks>
        public static IEnumerable<T> MyCast<T>(this IEnumerable collection)
        {
            foreach (var item in collection)
                yield return (T)item;
        }
    }
}
