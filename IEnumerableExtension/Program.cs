﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IEnumerableExtension
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] list1 = { 44, 26, 92, 30, 71, 38 };
            int[] list2 = { 39, 59, 83, 47, 26, 4, 30 };

            Console.WriteLine(" ---- list1 ---- ");
            foreach (int item in list1)
                Console.WriteLine(item);
            Console.WriteLine();

            Console.WriteLine(" ---- list2 ---- ");
            foreach (int item in list2)
                Console.WriteLine(item);
            Console.WriteLine();

            IEnumerable<int> where = list1.MyWhere(x => x > 40);
            Console.WriteLine(" ---- list1 where ( x > 40 ) ---- ");
            foreach (int item in where)
                Console.WriteLine(item);
            Console.WriteLine();

            IEnumerable<int> intersect = list1.MyIntersect(list2);
            Console.WriteLine(" ---- list1 intersect list2 ---- ");
            foreach (int item in intersect)
                Console.WriteLine(item);
            Console.WriteLine();

            bool all = list1.MyAll(x => x > 30);
            Console.WriteLine(" ---- list1 all (x > 30) ---- ");
            Console.WriteLine(all);
            Console.WriteLine();

            bool any = list1.MyAny(x => x > 30);
            Console.WriteLine(" ---- list1 any (x > 30) ---- ");
            Console.WriteLine(any);
            Console.WriteLine();

            IEnumerable<int> select = list1.MySelect(x => x * 2);
            Console.WriteLine(" ---- list1 select (x * 2) ---- ");
            foreach (int item in select)
                Console.WriteLine(item);
            Console.WriteLine();

            Console.WriteLine(" ---- petOwners select pets ---- ");
            PetOwner[] petOwners =
                { new PetOwner { Name="Higa, Sidney",
                      Pets = new List<string>{ "Scruffy", "Sam" } },
                  new PetOwner { Name="Ashkenazi, Ronen",
                      Pets = new List<string>{ "Walker", "Sugar" } },
                  new PetOwner { Name="Price, Vernette",
                      Pets = new List<string>{ "Scratches", "Diesel" } } };
            IEnumerable<string> selectmany = petOwners.MySelectMany(petOwner => petOwner.Pets);
            foreach (string pet in selectmany)
                Console.WriteLine(pet);
            Console.WriteLine();

            IEnumerable<int> skip = list1.MySkip(3);
            Console.WriteLine(" ---- list1 skip 3 ---- ");
            foreach (int item in skip)
                Console.WriteLine(item);
            Console.WriteLine();

            IEnumerable<int> take = list1.MyTake(3);
            Console.WriteLine(" ---- list1 take 3 ---- ");
            foreach (int item in take)
                Console.WriteLine(item);
            Console.WriteLine();

            int first = list1.MyFirst();
            Console.WriteLine(" ---- list1 first ---- ");
            Console.WriteLine(first);
            Console.WriteLine();

            int[] empty = { };
            int firstOrDefault = empty.MyFirstOrDefault();
            Console.WriteLine(" ---- empty arr first or default ---- ");
            Console.WriteLine(firstOrDefault);
            Console.WriteLine();

            System.Collections.ArrayList fruits = new System.Collections.ArrayList();
            fruits.Add(1);
            fruits.Add(2);
            fruits.Add(3);
            IEnumerable<int> cast = fruits.MyCast<int>();
            Console.WriteLine(" ---- arr cast to int ---- ");
            foreach (int item in cast)
                Console.WriteLine(item);
            Console.WriteLine();


            return;
        }
    }
    class PetOwner
    {
        public string Name { get; set; }
        public List<String> Pets { get; set; }
    }
}
